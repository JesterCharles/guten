const express = require("express");

const app = express();
let counter = 0;

app.get("/guten", (req, res) => {
	console.log("Hello I just logged");
	res.send("Gutentag" + ++counter);
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.log("App started"));
